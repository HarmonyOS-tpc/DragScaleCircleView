/**
 * Copyright 2015~2016, hpfs0.
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this work except in compliance with the License.
 * You may obtain a copy of the License in the LICENSE file, or at:
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language
 * governing permissions and limitations under the License.
 */
package com.rori.zenvo.dragscalecircleview;

import com.rori.zenvo.util.AttrUtils;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.Texture;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;
import ohos.multimodalinput.event.TouchEvent;

public class DragScaleCircleView extends Component {
    private PixelMap orgPixelMap;
    private PixelMap maskCanvas;

    private Boolean mHasGuideLine;

    private float mGuideLineSize;

    private int mGuideLineColor;

    private float mBorderSize;

    private int mBorderColor;

    private float mCenterPointX, mCenterPointY;

    private float mRadius;

    private RectFloat mBitmapRect = new RectFloat();

    private Paint mBorderPaint = new Paint();
    private Paint mSurroundingAreaOverlayPaint = new Paint();
    private Paint mHandlePaint = new Paint();
    private Paint mGuideLinePaint = new Paint();

    private float mHandleRadius;

    private int mHandleMode;

    private boolean isWidthWrap = false;
    private boolean isHeightWrap = false;
    private boolean isInit = false;

    private float mWidth;
    private float mHeight;
    private float offsetX;
    private float offsetY;

    private float circleCenterX = -1;
    private float circleCenterY = -1;
    private float circleRadius = -1;
    private float ratio;

    private float touchX;
    private float touchY;
    private int touchPose = 0;

    public DragScaleCircleView(Context context) {
        this(context, null, null);
    }

    public DragScaleCircleView(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public DragScaleCircleView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }

    private void init(AttrSet attrSet) {
        mHasGuideLine = AttrUtils.getBooleanFromAttr(attrSet, "hasGuideLine", true);
        mGuideLineSize = AttrUtils.getDimensionFromAttr(attrSet, "guideLineSize", dp2px(1));
        mGuideLineColor = AttrUtils.getColorFromAttr(attrSet, "guideLineColor", 0xAAFFFFFF);
        mBorderColor = AttrUtils.getColorFromAttr(attrSet, "borderColor", 0xAAFFFFFF);
        mBorderSize = AttrUtils.getColorFromAttr(attrSet, "borderSize", dp2px(3));
        Element element = AttrUtils.getElementFromAttr(attrSet, "imgSrc", null);
        if (element != null) {
            orgPixelMap = ((PixelMapElement) element).getPixelMap();
        }
        if (getLayoutConfig().height == ComponentContainer.LayoutConfig.MATCH_CONTENT) {
            isHeightWrap = true;
        }
        if (getLayoutConfig().width == ComponentContainer.LayoutConfig.MATCH_CONTENT) {
            isWidthWrap = true;
        }
        mSurroundingAreaOverlayPaint.setAntiAlias(true);
        mSurroundingAreaOverlayPaint.setStyle(Paint.Style.FILL_STYLE);
        mSurroundingAreaOverlayPaint.setColor(new Color(0x7f000000));

        mBorderPaint.setAntiAlias(true);
        mBorderPaint.setColor(new Color(mBorderColor));
        mBorderPaint.setStyle(Paint.Style.STROKE_STYLE);
        mBorderPaint.setStrokeWidth(mBorderSize);

        mHandlePaint.setAntiAlias(true);
        mHandlePaint.setColor(Color.WHITE);
        mHandlePaint.setStyle(Paint.Style.FILL_STYLE);

        mGuideLinePaint.setAntiAlias(true);
        mGuideLinePaint.setColor(new Color(mGuideLineColor));
        mGuideLinePaint.setStyle(Paint.Style.FILL_STYLE);
        mGuideLinePaint.setStrokeWidth(mGuideLineSize);
        setTouchEvent();
        draw();
    }

    private void setTouchEvent() {
        setTouchEventListener((component, touchEvent) -> {
            if (touchEvent.getPointerCount() == 1 && touchEvent.getAction() == TouchEvent.PRIMARY_POINT_DOWN) {
                touchX = getTouchX(touchEvent, 0);
                touchY = getTouchY(touchEvent, 0);
                float distance = calculateDistance(touchX, touchY);
                if (Math.abs(distance - circleRadius) < dp2px(5)) {
                    touchPose = 1;
                } else if (distance < circleRadius) {
                    touchPose = 2;
                }
                invalidate();
            } else if (touchEvent.getAction() == TouchEvent.POINT_MOVE) {
                if (touchPose == 1) {
                    touchX = getTouchX(touchEvent, 0);
                    touchY = getTouchY(touchEvent, 0);
                    float distance = calculateDistance(touchX, touchY);
                    if (circleCenterX - distance < offsetX) {
                        distance = circleCenterX - offsetX;
                    }
                    if (circleCenterY - distance < offsetY) {
                        distance = circleCenterY - offsetY;
                    }
                    if (circleCenterX + distance > mWidth - offsetX) {
                        distance = mWidth - offsetX - circleCenterX;
                    }
                    if (circleCenterY + distance > mHeight - offsetY) {
                        distance = mHeight - offsetY - circleCenterY;
                    }
                    if (distance < dp2px(15)) {
                        distance = dp2px(15);
                    }
                    if (mWidth < dp2px(30) || mHeight < dp2px(30)) {
                        distance = Math.min(mWidth, mHeight) / 2;
                    }
                    circleRadius = distance;
                    invalidate();
                } else if (touchPose == 2) {
                    circleCenterX += getTouchX(touchEvent, 0) - touchX;
                    circleCenterY += getTouchY(touchEvent, 0) - touchY;
                    if (circleCenterX - circleRadius < offsetX) {
                        circleCenterX = offsetX + circleRadius;
                    }
                    if (circleCenterY - circleRadius < offsetY) {
                        circleCenterY = offsetY + circleRadius;
                    }
                    if (circleCenterX + circleRadius > mWidth - offsetX) {
                        circleCenterX = mWidth - offsetX - circleRadius;
                    }
                    if (circleCenterY + circleRadius > mHeight - offsetY) {
                        circleCenterY = mHeight - offsetY - circleRadius;
                    }
                    touchX = getTouchX(touchEvent, 0);
                    touchY = getTouchY(touchEvent, 0);
                    invalidate();
                }
            } else if (touchEvent.getPointerCount() == 1 && touchEvent.getAction() == TouchEvent.PRIMARY_POINT_UP) {
                touchPose = 0;
                invalidate();
            }

            return true;
        });
    }

    private void calculateSize() {
        int width = getWidth();
        int height = getHeight();
        if (isWidthWrap || isHeightWrap) {
            ComponentContainer.LayoutConfig layoutConfig = getLayoutConfig();
            if (orgPixelMap != null) {
                if (isWidthWrap) {
                    if (width > 0) {
                        width = Math.min(orgPixelMap.getImageInfo().size.width, width);
                    }else {
                        width = orgPixelMap.getImageInfo().size.width;
                    }
                }
                if (isHeightWrap) {
                    if (height > 0) {
                        height = Math.min(orgPixelMap.getImageInfo().size.height, height);
                    }else {
                        height = orgPixelMap.getImageInfo().size.height;
                    }
                }
            } else {
                if (isWidthWrap) {
                    width = 0;
                }
                if (isHeightWrap) {
                    height = 0;
                }
            }
            if (isWidthWrap && layoutConfig.width != width || isHeightWrap && layoutConfig.height != height) {
                if (isWidthWrap) {
                    layoutConfig.width = width;
                }
                if (isHeightWrap) {
                    layoutConfig.height = height;
                }
                setLayoutConfig(layoutConfig);
            }
        }
        mWidth = width;
        mHeight = height;
        calculateOffset(width, height);
    }


    private void draw() {
        addDrawTask((component, canvas) -> {
            calculateSize();
            canvas.drawPixelMapHolderRect(new PixelMapHolder(orgPixelMap), new RectFloat(
                    0, 0, orgPixelMap.getImageInfo().size.width, orgPixelMap.getImageInfo().size.height
            ), new RectFloat(offsetX, offsetY, mWidth - offsetX, mHeight - offsetY), new Paint());

            canvas.drawRect(new RectFloat(offsetX, offsetY, mWidth - offsetX, mHeight - offsetY), mSurroundingAreaOverlayPaint);

            if (circleCenterX == -1 || circleCenterY == -1 || circleRadius == -1) {
                circleCenterX = mWidth / 2;
                circleCenterY = mHeight / 2;
                circleRadius = mWidth > mHeight ? mHeight * 0.3f : mWidth * 0.3f;
            }

            ratio = orgPixelMap.getImageInfo().size.width * 1.0f / (mWidth - offsetX * 2);

            canvas.drawPixelMapHolderCircleShape(new PixelMapHolder(orgPixelMap),
                    new RectFloat((circleCenterX - circleRadius - offsetX) * ratio, (circleCenterY - circleRadius - offsetY) * ratio,
                            (circleCenterX + circleRadius - offsetX) * ratio, (circleCenterY + circleRadius - offsetY) * ratio),
                    circleCenterX, circleCenterY, circleRadius
            );
            canvas.drawCircle(circleCenterX, circleCenterY, circleRadius, mBorderPaint);
            if (touchPose > 0 && mHasGuideLine) {
                float tan45 = (float) Math.sin(Math.toRadians(45));
                canvas.drawLine(new Point(circleCenterX - circleRadius, circleCenterY), new Point(circleCenterX + circleRadius, circleCenterY), mGuideLinePaint);
                canvas.drawLine(new Point(circleCenterX, circleCenterY - circleRadius), new Point(circleCenterX, circleCenterY + circleRadius), mGuideLinePaint);
                canvas.drawLine(new Point(circleCenterX - circleRadius * tan45, circleCenterY - circleRadius * tan45), new Point(circleCenterX + circleRadius * tan45, circleCenterY - circleRadius * tan45), mGuideLinePaint);
                canvas.drawLine(new Point(circleCenterX - circleRadius * tan45, circleCenterY - circleRadius * tan45), new Point(circleCenterX - circleRadius * tan45, circleCenterY + circleRadius * tan45), mGuideLinePaint);
                canvas.drawLine(new Point(circleCenterX + circleRadius * tan45, circleCenterY + circleRadius * tan45), new Point(circleCenterX + circleRadius * tan45, circleCenterY - circleRadius * tan45), mGuideLinePaint);
                canvas.drawLine(new Point(circleCenterX + circleRadius * tan45, circleCenterY + circleRadius * tan45), new Point(circleCenterX - circleRadius * tan45, circleCenterY + circleRadius * tan45), mGuideLinePaint);
            }

            if (touchPose == 1) {
                canvas.drawCircle(circleCenterX - circleRadius, circleCenterY, dp2px(5), mHandlePaint);
                canvas.drawCircle(circleCenterX + circleRadius, circleCenterY, dp2px(5), mHandlePaint);
                canvas.drawCircle(circleCenterX, circleCenterY - circleRadius, dp2px(5), mHandlePaint);
                canvas.drawCircle(circleCenterX, circleCenterY + circleRadius, dp2px(5), mHandlePaint);
            }
        });
    }

    private float calculateDistance(float touchX, float touchY) {
        float x = Math.abs(touchX - circleCenterX);
        float y = Math.abs(touchY - circleCenterY);
        return (float) Math.sqrt(x * x + y * y);
    }

    private void calculateOffset(int width, int height) {
        if (orgPixelMap == null || width * orgPixelMap.getImageInfo().size.height == height * orgPixelMap.getImageInfo().size.width) {
            offsetX = 0;
            offsetY = 0;
        } else if (width * 1.0f / orgPixelMap.getImageInfo().size.width > height * 1.0f / orgPixelMap.getImageInfo().size.height) {
            offsetX = (width - height * orgPixelMap.getImageInfo().size.width * 1.0f / orgPixelMap.getImageInfo().size.height) / 2;
            offsetY = 0;
        } else if (width * 1.0f / orgPixelMap.getImageInfo().size.width < height * 1.0f / orgPixelMap.getImageInfo().size.height) {
            offsetX = 0;
            offsetY = (height - width * orgPixelMap.getImageInfo().size.height * 1.0f / orgPixelMap.getImageInfo().size.width) / 2;
        }
    }

    private float getTouchX(TouchEvent touchEvent, int index) {
        float touchX = 0;
        if (touchEvent.getPointerCount() > index) {
            int[] xy = getLocationOnScreen();
            if (xy != null && xy.length == 2) {
                touchX = touchEvent.getPointerScreenPosition(index).getX() - xy[0];
            } else {
                touchX = touchEvent.getPointerPosition(index).getX();
            }
        }
        return touchX;
    }

    private float getTouchY(TouchEvent touchEvent, int index) {
        float touchY = 0;
        if (touchEvent.getPointerCount() > index) {
            int[] xy = getLocationOnScreen();
            if (xy != null && xy.length == 2) {
                touchY = touchEvent.getPointerScreenPosition(index).getY() - xy[1];
            } else {
                touchY = touchEvent.getPointerPosition(index).getY();
            }
        }
        return touchY;
    }

    public PixelMap getCroppedCircleBitmap(boolean isOrgPixel) {
        if (isOrgPixel) {
            PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
            initializationOptions.size = new Size((int) (circleRadius * ratio * 2), (int) (circleRadius * ratio * 2));
            initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
            PixelMap pixelMap = PixelMap.create(initializationOptions);
            Canvas canvas = new Canvas(new Texture(pixelMap));
            canvas.drawPixelMapHolderCircleShape(new PixelMapHolder(orgPixelMap),
                    new RectFloat((circleCenterX - circleRadius - offsetX) * ratio, (circleCenterY - circleRadius - offsetY) * ratio,
                            (circleCenterX + circleRadius - offsetX) * ratio, (circleCenterY + circleRadius - offsetY) * ratio),
                    circleRadius * ratio, circleRadius * ratio, circleRadius * ratio
            );
            return pixelMap;
        } else {
            PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
            initializationOptions.size = new Size((int) circleRadius * 2, (int) circleRadius * 2);
            initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
            PixelMap pixelMap = PixelMap.create(initializationOptions);
            Canvas canvas = new Canvas(new Texture(pixelMap));
            canvas.drawPixelMapHolderCircleShape(new PixelMapHolder(orgPixelMap),
                    new RectFloat((circleCenterX - circleRadius - offsetX) * ratio, (circleCenterY - circleRadius - offsetY) * ratio,
                            (circleCenterX + circleRadius - offsetX) * ratio, (circleCenterY + circleRadius - offsetY) * ratio),
                    circleRadius, circleRadius, circleRadius
            );
            return pixelMap;
        }
    }

    public PixelMap getCroppedCircleBitmap() {
        return getCroppedCircleBitmap(true);
    }

    public Boolean getHasGuideLine() {
        return mHasGuideLine;
    }

    public void setHasGuideLine(Boolean hasGuideLine) {
        this.mHasGuideLine = hasGuideLine;
        invalidate();
    }

    public float getGuideLineSize() {
        return mGuideLineSize;
    }

    public void setGuideLineSize(float guideLineSize) {
        this.mGuideLineSize = guideLineSize;
        this.mGuideLinePaint.setStrokeWidth(mGuideLineSize);
        invalidate();
    }

    public int getGuideLineColor() {
        return mGuideLineColor;
    }

    public void setGuideLineColor(int guideLineColor) {
        this.mGuideLineColor = guideLineColor;
        this.mGuideLinePaint.setColor(new Color(guideLineColor));
        invalidate();
    }

    public float getBorderSize() {
        return mBorderSize;
    }

    public void setBorderSize(float borderSize) {
        this.mBorderSize = borderSize;
        mBorderPaint.setStrokeWidth(mBorderSize);
        invalidate();
    }

    public int getBorderColor() {
        return mBorderColor;
    }

    public void setBorderColor(int borderColor) {
        this.mBorderColor = borderColor;
        mBorderPaint.setColor(new Color(mBorderColor));
        invalidate();
    }

    public void setImageDrawable(Element element) {
        if (element != null) {
            orgPixelMap = ((PixelMapElement) element).getPixelMap();
            invalidate();
        }
    }

    public void setImagePixelMap(PixelMap pixelMap) {
        orgPixelMap = pixelMap;
        invalidate();
    }

    private int dp2px(float dp) {
        return (int) (getResourceManager().getDeviceCapability().screenDensity / 160 * dp);
    }
}
