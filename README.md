# demo

<img src="art/demo.gif" width="40%"/>

# dragScaleCircleView

# Using:

```groovy
    dependencies{
       implementation 'io.openharmony.tpc.thirdlib:DragScaleCircleView:1.0.1'
   }
```

```xml
    <com.rori.zenvo.dragscalecircleview.DragScaleCircleView
         ohos:id="$+id:dragscalecircleview"
         ohos:height="120vp"
         ohos:width="300vp"
         app:imgSrc="$media:img1"
         app:hasGuideLine="false"
    />
```

```java
    //获取截图图片
    PixelMap pixelMap = dragScaleCircleView.getCroppedCircleBitmap();
```

# API:
## class DragScaleCircleView
**public PixelMap getCroppedCircleBitmap(boolean isOrgPixel)**
- description: get cropped circle bitmap and the bitmap is original pixel

**public PixelMap getCroppedCircleBitmap()**
- description: get cropped circlr bitmap

**public Boolean getHasGuideLine()**
- description: has guide line

**public void setHasGuideLine(Boolean hasGuideLine)**
- description: set has guide line

**public float getGuideLineSize()**
- description: get guide line size

**public void setGuideLineSize(float guideLineSize)**
- description: set guide line size

**public int getGuideLineColor()**
- description: get guide line color

**public void setGuideLineColor(int guideLineColor)**
- description: set guide line color

**public float getBorderSize()**
- description: get border size

**public void setBorderSize(float borderSize)**
- description: set border size

**public int getBorderColor()**
- description: get border color

**public void setBorderColor(int borderColor)**
- description: set border color

**public void setImageDrawable(Element element)**
- description: set original image by element

**public void setImagePixelMap(PixelMap pixelMap)**
- description: set original image by pixelmap

# AttrSet:

|name|format|description|
|:---:|:---:|:---:|
| hasGuideLine | boolean | set the flag of circle window's guide line diplay/not display
| guideLineSize | float | set the size of circle window's guide line
| guideLineColor | integer | set the color of circle window's guide line
| borderSize | float | set the size of circle window's border line
| borderColor | integer | set the color of circle window's border line
| imgSrc | element | set original image

## License

    Copyright 2015~2016 hpfs0

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.