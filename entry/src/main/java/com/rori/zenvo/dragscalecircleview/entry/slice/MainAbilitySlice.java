package com.rori.zenvo.dragscalecircleview.entry.slice;

import com.rori.zenvo.dragscalecircleview.DragScaleCircleView;
import com.rori.zenvo.dragscalecircleview.entry.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.media.image.PixelMap;
import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.util.ArrayList;
import java.util.List;

public class MainAbilitySlice extends AbilitySlice {
    private DragScaleCircleView dragScaleCircleView;
    private Image image;
    private Text showClip;
    private ToggleButton showGuide;
    private DiscreteSeekBar guideColor;
    private DiscreteSeekBar guideSize;
    private DiscreteSeekBar borderColor;

    private List<Integer> colors = new ArrayList<>();
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        colors.add(0xAAFFFFFF);
        colors.add(0xffff4444);
        colors.add(0xff99cc00);
        colors.add(0xffaa66cc);
        colors.add(0xff33b5e5);
        colors.add(0xffffbb33);

        image = (Image)findComponentById(ResourceTable.Id_image);
        dragScaleCircleView = (DragScaleCircleView)findComponentById(ResourceTable.Id_dragscalecircleview);
        showGuide = (ToggleButton)findComponentById(ResourceTable.Id_show_guide);
        guideColor = (DiscreteSeekBar)findComponentById(ResourceTable.Id_guide_color);
        guideSize = (DiscreteSeekBar)findComponentById(ResourceTable.Id_guide_size);
        borderColor = (DiscreteSeekBar)findComponentById(ResourceTable.Id_border_color);
        showClip = (Text)findComponentById(ResourceTable.Id_show_clip);

        showClip.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                PixelMap pixelMap = dragScaleCircleView.getCroppedCircleBitmap();
                image.setPixelMap(pixelMap);
            }
        });

        showGuide.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton absButton, boolean b) {
                if (showGuide.isChecked()) {
                    dragScaleCircleView.setHasGuideLine(true);
                }else {
                    dragScaleCircleView.setHasGuideLine(false);
                }
            }
        });
        guideColor.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar discreteSeekBar, int i, boolean b) {
                int color = colors.get(i);
                guideColor.setThumbColor(color, color, color);
                guideColor.setScrubberColor(color);
                dragScaleCircleView.setGuideLineColor(color);
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar discreteSeekBar) {
            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar discreteSeekBar) {
            }
        });

        borderColor.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar discreteSeekBar, int i, boolean b) {
                int color = colors.get(i);
                borderColor.setThumbColor(color, color, color);
                borderColor.setScrubberColor(color);
                dragScaleCircleView.setBorderColor(color);
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar discreteSeekBar) {
            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar discreteSeekBar) {
            }
        });

        guideSize.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {
            @Override
            public void onProgressChanged(DiscreteSeekBar discreteSeekBar, int i, boolean b) {
                dragScaleCircleView.setGuideLineSize(i);
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar discreteSeekBar) {
            }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar discreteSeekBar) {
            }
        });

    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
