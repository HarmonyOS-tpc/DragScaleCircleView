package org.adw.library.widgets.discreteseekbar.internal.drawable;

import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.render.BlurDrawLooper;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import org.adw.library.widgets.discreteseekbar.internal.AttrUtils;

public class MarkerDrawable {
    private static final int ANIMATION_DURATION = 250;
    private float mClosedStateSize;

    private Paint paint = new Paint();
    private Paint textPaint = new Paint();
    private int mPressedColor;
    private int mColor;
    private int indicatorElevation;
    private int textSize;
    private int textColor;
    private int shadowColor;
    private int shadowRadius;
    private int shadowDx;
    private int shadowDy;

    private AnimatorValue animatorValue = new AnimatorValue();

    public MarkerDrawable(Context context, AttrSet attrSets) {
        mColor = AttrUtils.getColorFromAttr(attrSets, "dsb_indicatorColor", 0xff009688);
        mPressedColor = AttrUtils.getColorFromAttr(attrSets, "dsb_indicatorPressedColor", mColor);
        indicatorElevation = AttrUtils.getDimensionFromAttr(attrSets, "dsb_indicatorElevation", dp2px(4, context));
        textColor = AttrUtils.getColorFromAttr(attrSets, "dsb_indicatorTextColor", Color.WHITE.getValue());
        textSize = AttrUtils.getDimensionFromAttr(attrSets, "dsb_indicatorTextSize", dp2px(12, context));
        shadowColor = AttrUtils.getColorFromAttr(attrSets, "dsb_indicatorTextShadowColor", 0x00000000);
        shadowRadius = AttrUtils.getDimensionFromAttr(attrSets, "dsb_indicatorTextShadowRadius", dp2px(1, context));
        shadowDx = AttrUtils.getDimensionFromAttr(attrSets, "dsb_indicatorTextShadowDx", dp2px(1, context));
        shadowDy = AttrUtils.getDimensionFromAttr(attrSets, "dsb_indicatorTextShadowDy", dp2px(1, context));
        paint.setAntiAlias(true);
        textPaint.setAntiAlias(true);
        textPaint.setColor(new Color(textColor));
        textPaint.setTextSize(textSize);
    }

    public void setColors(int color, int pressedColor) {
        mColor = color;
        mPressedColor = pressedColor;
    }

    public void doDraw(Canvas canvas, float x, float y,float offsetY,int windowsY,String text,float fraction,float radius,int thumbSize) {

        float tan45 = (float) Math.sin(Math.toRadians(45));
        float startY = y - tan45 * 2 * radius + offsetY;
        radius = thumbSize/2 + (radius - thumbSize/2) * fraction;
        y = y + (startY - y) * fraction -windowsY;
        BlurDrawLooper blurDrawLooper = new BlurDrawLooper(indicatorElevation, 0, 0, new Color(0x10000000));
        paint.setBlurDrawLooper(blurDrawLooper);
        paint.setColor(new Color(getAnimateColor(mColor,mPressedColor,fraction).asArgbInt()));
        Path path = new Path();
        path.moveTo(new Point(x - radius * tan45, y + radius * tan45));
        path.lineTo(new Point(x, y + radius + (radius * 2 * tan45 - radius) * fraction));
        path.lineTo(new Point(x + radius * tan45, y + radius * tan45));
        path.addArc(new RectFloat(x - radius, y - radius, x + radius, y + radius), 45, -270);
        path.close();
        canvas.drawPath(path, paint);

        if(fraction >= 0.9) {
            float sLength = textPaint.measureText(text);
            BlurDrawLooper textBlurDrawLooper = new BlurDrawLooper(shadowRadius, shadowDx, shadowDy, new Color(shadowColor));
            textPaint.setBlurDrawLooper(textBlurDrawLooper);
            canvas.drawText(textPaint, text, x - sLength / 2, y + textSize * 0.5f);
        }
    }

    public RgbColor getAnimateColor(int from, int to, float v) {
        RgbColor a = RgbColor.fromArgbInt(from);
        RgbColor b = RgbColor.fromArgbInt(to);
        int red = (int) (a.getRed() + (b.getRed() - a.getRed()) * v);
        int blue = (int) (a.getBlue() + (b.getBlue() - a.getBlue()) * v);
        int green = (int) (a.getGreen() + (b.getGreen() - a.getGreen()) * v);
        int alpha = (int) (a.getAlpha() + (b.getAlpha() - a.getAlpha()) * v);
        RgbColor mCurrentColorRgb = new RgbColor(red, green, blue, alpha);
        return mCurrentColorRgb;
    }

    private int dp2px(float dp, Context context) {
        return (int) (context.getResourceManager().getDeviceCapability().screenDensity / 160 * dp);
    }
}
